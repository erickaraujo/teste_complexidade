public class Fibonacci {

    public static void main(String[] args) {
        fibonacci_iterativo(10);

        System.out.println("recursivo:" + fibonacci_recursivo(10));
    }

    public static void fibonacci_iterativo(int termo){
        int     fib = 0,
                old_number = 0,
                new_number = 1;

        for(int i = 1; i <= termo; i++){
            old_number = new_number + fib;
            new_number = fib;
            fib = old_number;
        }
            System.out.println("Iterativo: " + fib);
    }

    public static int fibonacci_recursivo(int termo){
        if (termo < 2){
            return termo;
        }
        else{
            return fibonacci_recursivo(termo-1) + fibonacci_recursivo(termo-2);
        }
    }
}
