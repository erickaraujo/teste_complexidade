import java.util.Arrays;

public class Matriz {

    public static void main(String[] args) {
        int[][] m1 = new int[4][4];
        int[][] m2 = new int[4][4];

        for(int i=1; i<m1.length; i++){
            for(int j=1; j<m2.length; j++){
                m1[i][j] = i+1;
                m2[i][j] = j+1;
            }
        }
        System.out.println("m1: " + Arrays.deepToString(m1) + "\n m2: " + Arrays.deepToString(m2));

        for(int i=1; i<m1.length; i++){
            for(int j=1; j<m2.length; j++){
                if(m1[i][j] == m2[i][j])
                    m1[i][j] = 0;
                else
                    m2[i][j] = 1;
            }
        }

        System.out.println("m1: " + Arrays.deepToString(m1) + "\n m2: " + Arrays.deepToString(m2));
    }
}
