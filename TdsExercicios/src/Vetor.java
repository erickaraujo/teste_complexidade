import java.util.ArrayList;
import java.util.Random;

public class Vetor {

    public static void main(String[] args) {

        ArrayList<Integer> lista_itera = gera_lista();
        ordenaIterativo(lista_itera);


        ArrayList<Integer> lista_recursivo = gera_lista();
        System.out.println("\n\n\npré: " + lista_recursivo);
        ordenaRecursivo(lista_recursivo, lista_recursivo.size());
        System.out.println("pós: " + lista_recursivo);
    }

    private static ArrayList<Integer> gera_lista(){
        ArrayList<Integer> lista_num = new ArrayList<>();

        Random random = new Random();

        int r = random.nextInt(10);
        while(lista_num.size() != 10){
            if(lista_num.contains(r)){
                r = random.nextInt(10);
            }
            else {
                lista_num.add(r);
            }
        }

        return lista_num;
    }

    private static void ordenaIterativo(ArrayList<Integer> lista){
        System.out.println("lista original:" + lista);

        for(int i = 0; i<lista.size();i++){
            int j = i;
            while(j>=0){
                if(j-1<0){
                    break;
                }
                int anterior = lista.get(j-1);
                int atual = lista.get(j);
                if(anterior > atual ){
                    lista.set(j-1, atual);
                    lista.set(j, anterior);
                }
                j--;
            }
        }

        System.out.println("Lista Ordenada: " + lista);
    }

    private static void ordenaRecursivo(ArrayList<Integer> lista, int tamanho){
        if(tamanho <= 1){
            return;
        }

        ordenaRecursivo(lista, tamanho-1);
        int anterior = lista.get(tamanho-1);
        int j = tamanho-2;
        while (j>=0 && lista.get(j) > anterior){
            lista.set(j+1, lista.get(j));
            j--;
        }

        lista.set(j+1, anterior);
    }
}
