public class Fatorial {
    public static void main(String[] args) {
        System.out.println("FATORIAL RECURSIVO!");
        System.out.println("Resultado Recursivo --> " + fatorial_recur(3));

        fatorial_itera(3);
    }

    private static void fatorial_itera(int numero){
        int     resultado = 0,
                i = numero -1;

        System.out.println("\nFATORIAL ITERATIVO!");
        if(numero <= 1){
            resultado = 1;
            System.out.println("resultado iterativo: " + resultado);
        }

        else {
            while (i > 0) {
                System.out.println("Fatorial: " + i);
                numero = numero * i;
                System.out.println("resultado iterativo: " + numero);
                i--;
            }
        }
    }

    private static int fatorial_recur(int numero){
        int resultado = 0;
        if(numero == 0){
            return 1;
        }
        else{
            System.out.println("fatorial: " + numero);
            return fatorial_recur(numero - 1) * numero;
        }
    }
}
